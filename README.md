# model_server

Serving Vision to Living Things.

## Summary

model_server is a service for on-demand computer vision, adapted specifically to image-based feedback in microscopy 
and other life sciences applications.  It abstracts image data access, persists machine learning models, 
and exposes an extensible API to facilitate low-latency analysis.  

## Install Git and miniforge
1. Install Miniforge for environment management:<br>https://github.com/conda-forge/miniforge/releases
2. Under the Start menu, open `Miniforge3 > Miniforge Prompt`

## Option 1: install model_server as a package:
1. Download the most recent version of the built package from:<br>https://git.embl.de/rhodes/model_server/-/packages/1280
2. (optional) activate the target conda environment: `mamba activate <target_environment>`
3. From the package repository https://git.embl.de/rhodes/model_server/-/packages/ download:
   - The most recent requirements.yml
   - The most recent .tar.bz2 file containing the built conda package
4. In a text editor, open requirements.yml and remove all but the "channels" and "dependencies" blocks, then save.
5. Change directories to the location of 'requirements.yml' and install dependencies:<br>`mamba env update -f requirements.yml`
6. Download the most recent .tar.bz2 file containing the built conda package from:<br>https://git.embl.de/rhodes/model_server/-/packages/1283
7. Change directories to the downloaded file and install model_server package: `mamba install model_server-<version>-py_0.tar.bz2`

## Option 2: install model_server from source:
1. Install Git:<br>https://git-scm.com/download/win
2. In the new terminal, clone the model_server repository:<br>`cd %userprofile%`<br>`git clone https://almf-staff:KJmFvyPRbpzoVZDqfMzV@git.embl.de/rhodes/model_server.git`
3. Create the target environment: `mamba env create --file requirements.yml --name model_server_env`
4. Activate the target environment: `mamba activate model_server_env`
5. Add the project source as a Python package: `pip install --no-deps -e .`

## To start the server:
1. From the Miniforge prompt, run `mamba activate <target_environment>`
2. Then run `python -m scripts.run_server --port 6221`
3. A browser window should appear, with basic status information.
