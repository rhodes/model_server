# SVLT: <u>S</u>erving <u>V</u>ision to <u>L</u>iving <u>T</u>hings

## Overview
SVLT is a service for on-demand computer vision, adapted specifically to life sciences applications that require 
rapid feedback as images are acquired.  It exposes APIs that make it easy for instruments to request 
sophisticated image analyses while experiments are running.  This is intended to accelerate advanced imaging, 
experimental manipulation of biological entities, and image feedback for the preparation and manufacturing of 
bioproducts.

## Key design goals

**Interoperability:** Image feedback is desired across diverse instrumentation and computing infrastructures. SVLTs 
service architecture makes it broadly usable in instruments, labs, clusters, and clouds.  

**Low latency:** SVLT is the opposite of a workflow manager, in that it prioritizes a single rapid answer over the 
throughput of batches of answer.  But its API is async, so clients can readily parallelize computation as needed.
Pipeline performance is automatically recorded to help accurately profile system performance.  

**Extensibility:** There will always be new tasks, formats, and models.  Core classes and modular pipelines make it 
as easy as possible to incorporate these into the project.

## Solutions to specific problems

**Problem:** Instrument control and data analysis thrive in very different environments.\
**Solution:** A FastAPI supports rapid communication between environments.  It also eases distributed deployment to 
dedicated on-prem hardware, HPC clusters, the cloud, etc.  MQTT is also implemented on an experimental basis for 
embedded and robotics applications.

**Problem:** Microscopes see heterogeneous objects in complex scenes.\
**Solution:** SVLT leverages cutting-edge machine learning frameworks.  It serves ML models for segmentation, the 
results of which are tabulated as RoiSet objects.  Queries of this set are then passed to patch- or scene-, 
context-aware object classification models e.g. YOLO.  And these results accumulate during sessions, paving the way 
for spatial statistics and advanced learning workflows.

**Problem:** Machine learning uses large in-memory models.  Loading them is a bottleneck.\
**Solution:** The service holds models in state, so they are only loaded once per session.  Hence, serial inference 
calls are usually very fast.

**Problem:** ML models are very particular about their input specification.\
**Solution:** SVLT wraps image data in Accessors with intuitive dimensions (height, width, depth, colorspace).  
Where possible, these are derived automatically from microscope file metadata.  Models then introspect their 
compatibility when attempting to run predictions.

**Problem:** It is hard to keep track of parameters in image analysis pipelines.\
**Solution:** Pipeline parameters are specified as JSON-serializable types with helpful docstrings.  These schemas 
are inheritable and discoverable by clients.  Intermediate data representations are persisted and can be exported during 
parameter-tuning campaigns.

**Problem:** ML frameworks are picky about their platform and dependencies.\
**Solution:** The project comprises core classes (data and model abstraction, base API) and framework-specific 
extensions on a monorepo.  Framework-platform pairs can then be built to spec with conda build.

## Components

### svlt.core.accessors
Abstractions of single- and multi-position image data, generally multichannel Z-stacks.  Data can either exist purely 
in-memory, such as the results of a pipeline operation, or be backed by image files. For example:<br>

```
>>> import numpy as np
>>> from svlt.core.accessors import InMemoryDataAccessor

>>> arr = np.array([[4, 4], [2, 3]], dtype='uint8')
>>> acc = InMemoryDataAccessor(arr)

>>> acc.shape_dict
{'Y': 2, 'X': 2, 'C': 1, 'Z': 1}

>>> acc.is_mask()
False

>>> acc.chroma == 1
True

>>> new_acc = acc.apply(lambda x: 2 * x)
>>> type(new_acc)
<class 'svlt.base.accessors.InMemoryDataAccessor'>

>>> new_acc.unique()
(array([4, 6, 8], dtype=uint8), array([1, 1, 2], dtype=int64))
```
Data accessors can be imported directly from microscope image file formats:
```
>>> from svlt.base.accessors import GenericImageFileAccessor
>>> facc = GenericImageFileAccessor.read('~/svlt/test_data/D3-selection-01.czi')
>>> facc.shape_dict
{'Y': 1274, 'X': 1274, 'C': 5, 'Z': 1}
>>> facc.get_channels([2, 3]).shape_dict
{'Y': 1274, 'X': 1274, 'C': 2, 'Z': 1}
>>> facc.pixel_scale_in_micrometers
{'X': 0.25074568288853993, 'Y': 0.25074568288853993}
```

### svlt.core.api
The core FastAPI methods, e.g. for configuring and restarting sessions, importing and exporting data accessors,
and interacting with generic models.

### svlt.core.models
Abstractions of models that generally are (1) loaded once then (2) called for inference many times during an experiment.

### svlt.core.pipelines.*
Implements classical, learned, and hybrid pipelines for image segmentation and analysis.  Each module is a single 
pipeline, comprising a main function that acts of accessors, its API endpoints, and a parameter model.

### svlt.core.roiset
The RoiSet data structure catalogs regions of interest (ROI) inside of images.  ROI metadata are queryable with the 
Pandas API.  Patches can be exported for visualization and annotation.

### svlt.core.session
This module represents the state of a "session" and is mainly called via the core FastAPI

## Examples

### Running unittests
1. Build and activate the conda environment as described in README.md
2. Copy test data from https://s3.embl.de/model-server-fixtures (inquire for access) to a local directory <data_root>
3. In the development shell or IDE, set an environmental variable UNITTEST_DATA_ROOT=<data_root>
4. From the project root, run `python -m unittest discover`

### Interactive server example
This involves manual clicking but covers the main steps that a client should call programmatically.
The same general sequence is covered in:
`tests.ilastik.test_ilastik.TestRoiSetWorkflowOverApi.test_ilastik_infer_pixel_probability`
1. Procure the test data as described previously 
2. Start the FastAPI server: `python scripts.run_server.py`
3. Wait for the browser to open with `http://127.0.0.1:8000/status`
4. Downlaod and open the configuration JSON if it does not appear automatically in the browser.
5. Copy the test image `D3-selection-01.czi` to the session input directory listed under `paths.inbound_images`
6. Note the location of `paths.outbound_images`
7. In the browser, navigate to `http://127.0.0.1:8000/docs`
8. Load an ilastik model:
   - under `/ilastik/seg/load/`, press "Try it out"
   - under "Request Body", copy the full local path to `ilastik/demo_px.ilp` in the "project_file" field
   - set "model_id" to "test_model"
   - set "duplicate" to false
   - click "Execute"
   - confirm that you receive a server response code of 200
9. Load the image as an in-memory accessor under `/accessors/read_from_file/`
   - set "filename" to `D3-selection-01.czi`
   - set "accessor_id" to 'test_acc'
   - again, "Execute" and confirm a status code of 200
10. Run inference on the image under `pipelines/segment`
   - set "accessor_id" to 'test_acc'
   - set "model_id" to "test_model"
   - "Execute" and confirm a status code of 200
   - The output mask should soon appear in the directory described in `paths.outbound_images`

## Extending SVLT

### Where to write extensions
1. If using the same dependencies as `svlt-core`, create a subpackage of `base.extensions`
2. If using different or new dependencies, create a new package that imports `svlt-core`
3. Write unittests in a subdirectory of `tests` with the same (sub)package name
4. For the time being, push to a branch starting with extension_ followed by the (sub)package name
5. Write command-line scripts in `scripts`, i.e. outside of the package's directory tree

### Extending models
1. First see if any existing model superclass matches the model's task.  E.g. if a model processes an image into a 
segmentation mask, inherit from `models.SemanticSegmentationModel`
2. Otherwise, inherit from `core.models.Model`
3. In the `.load()` method, implement any resource-intensive code that loads a model once for subsequent use.
4. In the `.infer()` method, implement any code that runs inference on the model.
5. If extensive setup parameters are used, specify these as a Pydantic model e.g. in 
`extensions.ilastiik.models.IlastikParams`

### Extending pipelines

Using `pipelines.segment` as a template, generally work from bottom to top:

1. Implement the main pipeline function ending in `*_pipeline()`.  
   - This function interacts with accessors and models as dictionaries of their python objects.  
   - E.g. parameter `example_accessor_id` becomes `accessors['example_accessor']` in the calling function,
   - and `example_model_id` populates `models['example_model']`, and so on.  
   - Instantiate, populate, and return a single `PipelineTrace` object with any meaningful intermediate accessors.  
   - The API will automatically assign an ID to the final accessor in the trace, and all intermediate accessors if 
keep_interm parameter is True.
2. Implement the API function and its API path.  
   - Other than renaming the function, its path, and the `Params` and `Record` subclasses, nothing here needs to change.
   - This receives a single `PipelineParams` object as input, automatically maps accessors and models from their ID 
strings to objects, passes them to `*_pipeline()`, and returns a single `PipelineRecord` as output.  
   - Fast API handles the parsing and validation of fields in the request and response.  
3. Optionally, sublass `PipelineRecord`, if additional parameters are expected from the pipeline.
4. Subclass `PipelineParams` with additional parameters that the pipeline needs.  
   - By default, single `accessor_id` and `model_id` fields are already defined.  
   - These and anything ending in `accessor_id` or `model_id` are automatically validated to check that their IDs match 
loaded models.  
   - Use a descriptive class name.  FastAPI uses this in a global schema, so it helps users understand the expected 
parameters.  
   - Use default values where possible.  Field validation can optionally use `pydantic.Field` classes for this.
   
### Testing

1. Write unittests that cover new models, pipeline functions, and API calls.
2. Where possible, use the existing test data described "Examples" > "Running unittests"
3. If adding test data or models, inquire about adding these to the distributed test data.
4. For new pipeline modules, it is advisable to both:
   - test `*_pipeline()` directly in a subclass of `unittest.TestCase`
   - and test FastAPI endpoints in a subclass of `conf.TestServerBaseClass`, by way of its `._get()` and `._put()` methods
5. Where possible, assertions should assure not just error-free functionality, but also test that the content of outputs 
is correct.
