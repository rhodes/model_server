from abc import ABC, abstractmethod
import os
from pathlib import Path

import numpy as np
import pandas as pd
from skimage.io import imread, imsave

import czifile
import nd2
import tifffile

from .process import is_mask, make_rgb, resample_to_8bit

class GenericImageDataAccessor(ABC):

    axes = 'YXCZ'

    @abstractmethod
    def __init__(self):
        """
        Abstract base class that exposes an interfaces for image data, irrespective of whether it is instantiated
        from file I/O or other means.  Enforces X, Y, C, Z dimensions in that order.
        """
        self.lazy = False
        pass

    @property
    def loaded(self):
        return self._data is not None

    @property
    def chroma(self):
        return self.shape_dict['C']

    @staticmethod
    def conform_data(data):
        if len(data.shape) > 4 or (0 in data.shape):
            raise DataShapeError(f'Cannot handle image with dimensions other than X, Y, C, and Z: {data.shape}')
        ones = [1 for i in range(0, 4 - len(data.shape))]
        return data.reshape(*data.shape, *ones)

    def is_3d(self):
        return True if self.shape_dict['Z'] > 1 else False

    def is_mask(self):
        return is_mask(self.data)

    def can_mask(self, acc):
        return self.is_mask() and self.shape == acc.get_mono(0).shape

    def get_channels(self, channels: list, mip: bool = False):
        carr = [int(c) for c in channels]
        if mip:
            nda = self.data.take(indices=carr, axis=self._ga('C')).max(axis=self._ga('Z'), keepdims=True)
            return self._derived_accessor(nda)
        else:
            nda = self.data.take(indices=carr, axis=self._ga('C'))
            return self._derived_accessor(nda)


    def get_zi(self, zi: int):
        """
        Return a new accessor of a specific z-coordinate
        """
        return self._derived_accessor(
            self.data.take(
                indices=[zi],
                axis=self._ga('Z')
            )
        )

    def get_mip(self):
        """
        Return a new accessor of maximum intensity projection (MIP) along z-axis
        """
        return self.apply(lambda x: x.max(axis=self._ga('Z'), keepdims=True))

    def get_mono(self, channel: int, mip: bool = False):
        return self.get_channels([channel], mip=mip)

    def get_z_argmax(self):
        return self.apply(lambda x: x.argmax(axis=self.get_axis('Z')))

    def get_focus_vector(self):
        return self.data.sum(axis=(0, 1, 2))

    @property
    def data_yx(self) -> np.ndarray:
        if not (self.chroma == 1 and self.nz == 1):
            raise InvalidDataShape('Can only return XY array from accessors with a single channel and single z-level')
        else:
            return self.data[:, :, 0, 0]

    @property
    def data_yxz(self) -> np.ndarray:
        if not self.chroma == 1:
            raise InvalidDataShape('Can only return XYZ array from accessors with a single channel')
        else:
            return self.data[:, :, 0, :]

    @property
    def data_yxcz(self) -> np.ndarray:
        return self.data

    @property
    def data_yxzc(self) -> np.ndarray:
        return np.moveaxis(
            self.data,
            [0, 1, 3, 2],
            [0, 1, 2, 3]
        )

    @property
    def data_mono(self) -> np.ndarray:
        if self.nz > 1:
            return self.data_yxz
        else:
            return self.data_yx

    def _gc(self, channels):
        return self.get_channels(list(channels))

    def unique(self):
        return np.unique(self.data, return_counts=True)

    @property
    def dtype_max(self):
        return np.iinfo(self.dtype).max

    @property
    def pixel_scale_in_micrometers(self):
        return {}

    @property
    def dtype(self):
        return self.data.dtype

    def write(self, fp: Path, mkdir=True, **kwargs) -> Path:
        return write_accessor_data_to_file(fp, self, mkdir=mkdir, **kwargs)

    def get_axis(self, ch):
        return self.axes.index(ch.upper())

    def _ga(self, arg):
        return self.get_axis(arg)

    def crop_hw(self, yxhw: tuple):
        """
        Return subset of data cropped in X and Y
        :param yxhw: tuple (Y, X, height, width)
        :return: InMemoryDataAccessor of size (H x W), starting at (Y, X)
        """
        y, x, h, w = yxhw
        return InMemoryDataAccessor(self.data[y: (y + h), x: (x + w), :, :])

    def crop_hwd(self, yxzhwd: tuple):
        """
        Return subset of data cropped in X and Y
        :param yxzhwd: tuple (Y, X, Z, height, width, depth)
        :return: InMemoryDataAccessor of size (H x W x D), starting at (Y, X, Z)
        """
        y, x, z, h, w, d = yxzhwd
        return InMemoryDataAccessor(self.data[y: (y + h), x: (x + w), :, z: (z + d)])

    @property
    def hw(self):
        """
        Get data height and width as a tuple
        :return: tuple of (Y, X) dimensions
        """
        return self.shape_dict['Y'], self.shape_dict['X']

    @property
    def nz(self):
        return self.shape_dict['Z']

    @property
    def data(self):
        """
        Return data as 4d with axes in order of Y, X, C, Z
        :return: np.ndarray
        """
        return self._data

    @property
    def shape(self):
        return self.data.shape

    @property
    def shape_dict(self):
        return dict(zip(('Y', 'X', 'C', 'Z'), self.data.shape))

    @staticmethod
    def _derived_accessor(data):
        """
        Create a new accessor given np.ndarray data; used for example in slicing operations
        """
        return InMemoryDataAccessor(data)

    def apply(self, func, params: dict = {}, preserve_dtype=True, mono=False):
        """
        Apply func to data and return as a new in-memory accessor
        :param func: function that receives and returns the same size np.ndarray
        :param params: (optional) dictionary of parameters to pass to function
        :param preserve_dtype: returned accessor gets the same dtype as self if True
        :param mono: check that accessor is mono and pass only YXZ data to func
        :return: InMemoryDataAccessor
        """
        if mono:
            if self.chroma != 1:
                raise DataShapeError(f'Expecting monochrome accessor when calling with mono=True')
            nda = func(self.data_mono, **params)
        else:
            nda = func(self.data, **params)
        if preserve_dtype:
            nda = nda.astype(self.dtype)
        if mono:
            return self._derived_accessor(np.expand_dims(nda, 2))
        else:
            return self._derived_accessor(nda)

    @property
    def info(self):
        if self.loaded:
            return {
                'shape_dict': self.shape_dict,
                'dtype': str(self.dtype),
                'filepath': '',
                'nbytes': self.data.nbytes,
            }
        else:
            return {
                'filepath': '',
            }

    def append_channels(self, acc):
        if self.dtype != acc.dtype:
            raise DataTypeError(f'Cannot append data of type {acc.dtype} to an accessor with type {self.dtype}')
        return self._derived_accessor(
            np.concatenate(
                (self.data, acc.data),
                axis=self._ga('C')
            )
        )

    def to_8bit(self):
        if self.dtype == 'uint8':
            return self
        else:
            return self.apply(resample_to_8bit, preserve_dtype=False)

class InMemoryDataAccessor(GenericImageDataAccessor):
    def __init__(self, data):
        self._data = self.conform_data(data)
        self.lazy = False

    @classmethod
    def from_mono(cls, data):
        if len(data.shape) == 2: # interpret as YX
            return cls(np.expand_dims(data, (2, 3)))
        if len(data.shape) == 3:
            return cls(np.expand_dims(data, 2))
        else:
            raise InvalidDataShape(f'Expecting either YX or YXZ monochromatic data')

class GenericImageFileAccessor(GenericImageDataAccessor): # image data is loaded from a file
    def __init__(self, fpath: Path, lazy=False):
        """
        Interface for image data that originates in an image file
        :param fpath: absolute path to image file
        :param kwargs: variable-length keyword arguments
        """
        if not os.path.exists(fpath):
            raise FileAccessorError(f'Could not find file at {fpath}')
        self.fpath = fpath

        self._data = None
        self._metadata = None
        self.lazy = lazy

        if not lazy:
            self.load()


    @abstractmethod
    def load(self) -> (np.ndarray, dict):
        pass

    @property
    def data(self):
        """
        Load from file if not loaded already; return data as 4d with axes in order of Y, X, C, Z
        """
        if self._data is None:
            self.load()
        return self._data

    @property
    def metadata(self):
        if self._metadata is None:
            self.load()
        return self._metadata

    @staticmethod
    def read(fp: Path):
        return generate_file_accessor(fp)

    @property
    def info(self):
        d = super().info
        d['filepath'] = self.fpath.__str__()
        return d

class TifSingleSeriesFileAccessor(GenericImageFileAccessor):
    def load(self):
        fpath = self.fpath

        try:
            tf = tifffile.TiffFile(fpath)
        except Exception:
            raise FileAccessorError(f'Unable to access data in {fpath}')

        if len(tf.series) != 1:
            raise DataShapeError(f'Expect only one series in {fpath}')

        se = tf.series[0]

        order = ['Y', 'X', 'C', 'Z']
        axs = [a for a in se.axes if a in order]
        da = se.asarray()

        if 'C' not in axs:
            axs.append('C')
            da = np.expand_dims(da, len(da.shape))

        if 'Z' not in axs:
            axs.append('Z')
            da = np.expand_dims(da, len(da.shape))

        yxcz = np.moveaxis(
            da,
            [axs.index(k) for k in order],
            [0, 1, 2, 3]
        )
        tf.close()
        self._data = self.conform_data(yxcz.reshape(yxcz.shape[0:4]))
        self._metadata = {}

class PngFileAccessor(GenericImageFileAccessor):
    def load(self):
        fpath = self.fpath

        try:
            arr = imread(fpath)
        except Exception:
            FileAccessorError(f'Unable to access data in {fpath}')

        if len(arr.shape) == 3: # rgb
            self._data = np.expand_dims(arr, 3)
        else: # mono
            self._data = np.expand_dims(arr, (2, 3))
        self._metadata = {}

class CziImageFileAccessor(GenericImageFileAccessor):
    """
    Image that is stored in a Zeiss .CZI file; may be multi-channel, and/or a z-stack, but not a time series
    or multiposition acquisition.  Read the whole file as a single accessor, no interaction with subblocks.
    """
    def load(self):
        try:
            cf = czifile.CziFile(self.fpath)
            metadata = cf.metadata(raw=False)
        except Exception:
            raise FileAccessorError(f'Unable to access CZI data in {self.fpath}')

        # check for incompatible compression type
        try:
            compmet = metadata['ImageDocument']['Metadata']['Information']['Image']['OriginalCompressionMethod']
        except KeyError:
            raise InvalidCziCompression('Could not find metadata key OriginalCompressionMethod')
        if compmet.upper() != 'UNCOMPRESSED':
            raise InvalidCziCompression(f'Unsupported compression method {compmet}')
        yxcz = self.get_yxcz(cf)
        self._data = self.conform_data(yxcz.reshape(yxcz.shape[0:4]))
        self._metadata = metadata
        cf.close()


    def get_yxcz(self, cf):
        sd = {ch: cf.shape[cf.axes.index(ch)] for ch in cf.axes}
        if (sd.get('S') and (sd['S'] > 1)) or (sd.get('T') and (sd['T'] > 1)):
            raise DataShapeError(f'Cannot handle image with multiple positions or time points: {sd}')

        idx = {k: sd[k] for k in ['Y', 'X', 'C', 'Z']}
        return np.moveaxis(
            cf.asarray(),
            [cf.axes.index(ch) for ch in idx],
            [0, 1, 2, 3]
        )

    @property
    def pixel_scale_in_micrometers(self):
        scale_meta = self.metadata['ImageDocument']['Metadata']['Scaling']['Items']['Distance']
        sc = {}
        for m in scale_meta:
            if m['DefaultUnitFormat'].encode() == b'\xc2\xb5m' and m['Id'] in self.shape_dict.keys():  # literal mu-m
                sc[m['Id']] = m['Value'] * 1e6
        return sc

class Nd2ImageFileAccessor(GenericImageFileAccessor):
    """
    An accessor backed by a Nikon .ND2 file
    """
    def load(self):
        fpath = self.fpath

        try:
            fh = nd2.ND2File(fpath)
        except Exception:
            raise FileAccessorError(f'Unable to access data in {fpath}')

        order = ['Y', 'X', 'C', 'Z']
        axs = [a for a in dict(fh.sizes) if a in order]
        da = fh.asarray()

        if 'C' not in axs:
            axs.append('C')
            da = np.expand_dims(da, len(da.shape))

        if 'Z' not in axs:
            axs.append('Z')
            da = np.expand_dims(da, len(da.shape))

        yxcz = np.moveaxis(
            da,
            [axs.index(k) for k in order],
            [0, 1, 2, 3]
        )
        fh.close()
        self._data = self.conform_data(yxcz.reshape(yxcz.shape[0:4]))
        self._metadata = {}


def write_accessor_data_to_file(fpath: Path, acc: GenericImageDataAccessor, mkdir=True, composite=False) -> bool:
    """
    Export an image accessor to file
    :param fpath: complete path including filename and extension
    :param acc: image accessor to be written
    :param mkdir: create any needed subdirectories in fpath if True
    :param composite: when exporting an ImageJ TIF, use composite channels if True
    :return: True
    """
    if 'P' in acc.shape_dict.keys():
        raise FileWriteError(f'Can only write single-position accessor to file')
    ext = fpath.suffix.upper()

    if mkdir:
        fpath.parent.mkdir(parents=True, exist_ok=True)

    if ext == '.PNG':
        if acc.dtype != 'uint8':
            raise FileWriteError(f'Invalid data type {acc.dtype}')
        if acc.chroma == 1:
            data = acc.data[:, :, 0, 0]
        elif acc.chroma == 2:  # add a blank blue channel
            data = make_rgb(acc.data)[:, :, :, 0]
        else:  # preserve RGB order
            data = acc.data[:, :, :, 0]
        imsave(fpath, data, check_contrast=False)
        return True

    elif ext in ['.TIF', '.TIFF']:
        zcyx= np.moveaxis(
            acc.data,  # yxcz
            [3, 2, 0, 1],
            [0, 1, 2, 3]
        )
        if acc.is_mask():
            if acc.dtype == 'bool':
                data = (zcyx * 255).astype('uint8')
            else:
                data = zcyx.astype('uint8')
            tifffile.imwrite(fpath, data, imagej=True)
        else:
            tifffile.imwrite(
                fpath,
                zcyx,
                imagej=True,
                metadata={'mode': 'composite'} if composite else None
            )

    else:
        raise FileWriteError(f'Unable to write data to file of extension {ext}')
    return fpath


def generate_file_accessor(fpath, **kwargs):
    """
    Given an image file path, return an image accessor, assuming the file is a supported format and represents
    a single position array, which may be single or multichannel, single plane or z-stack.
    """
    if str(fpath).upper().endswith('.TIF') or str(fpath).upper().endswith('.TIFF'):
        return TifSingleSeriesFileAccessor(fpath, **kwargs)
    elif str(fpath).upper().endswith('.CZI'):
        return CziImageFileAccessor(fpath, **kwargs)
    elif str(fpath).upper().endswith('.ND2'):
        return Nd2ImageFileAccessor(fpath, **kwargs)
    elif str(fpath).upper().endswith('.PNG'):
        return PngFileAccessor(fpath, **kwargs)
    else:
        raise FileAccessorError(f'Could not match a file accessor with {fpath}')

class AccessorSet(object):
    pass

class AccessorSetMember(InMemoryDataAccessor):
    pass

class MultiPositionCziFileAccessorSet(AccessorSet):
    def __init__(self, fpath, lazy=True):
        pd.options.mode.chained_assignment = None  # silence a pandas warning
        self.fpath = fpath
        super().__init__()

        with czifile.CziFile(fpath) as fh:
            sd = {ch: fh.shape[fh.axes.index(ch)] for ch in fh.axes}

            if sd.get('S') is None or sd['S'] == 1:
                raise DataShapeError(f'Expecting multiposition CZI file')
            for k in sd.keys():
                if k not in 'SYXCZ' and sd[k] > 1:
                    raise DataShapeError(f'Unrecognized dimension great than one: {k} in {sd}')

            self.subblock_coords = pd.DataFrame(
                [dict(zip(fh.axes, s.start)) for s in fh.filtered_subblock_directory]
            )
            self.count = sd['S']
        self._data = [None for _ in range(0, self.count)]
        self.lazy = lazy

        if not lazy:
            self.get_accessors()

    def tczyx_iat(self, pi):
        with czifile.CziFile(self.fpath) as fh:
            coords = self.subblock_coords
            df_pi = coords.loc[coords['S'] == pi, :]
            df_pi['data'] = [fh.filtered_subblock_directory[i].data_segment().data() for i in df_pi.index]

            hu = df_pi['data'].apply(lambda x: x.shape[fh.axes.index('Y')]).unique()
            wu = df_pi['data'].apply(lambda x: x.shape[fh.axes.index('X')]).unique()
            if len(hu) > 1 or len(wu) > 1:
                raise DataShapeError(f'Subblocks are not the same size: width={wu}, height={hu}')

            nt, nc, nz = tuple(df_pi[['T', 'C', 'Z']].max() + 1)
            tczyx = np.zeros((nt, nc, nz, hu[0], wu[0]), dtype=fh.dtype)
            for ti, ci, zi in np.ndindex(nt, nc, nz):
                q = np.logical_and(df_pi['T'] == ti, df_pi['C'] == ci, df_pi['Z'] == zi)
                sbd = df_pi.loc[q, 'data']
                if len(sbd) > 1:
                    raise DataShapeError(f'Found more than one subblock at (P, T, C, Z) = ({pi}, {ti}, {ci}, {zi}')
                tczyx[ti, ci, zi, :, :] = np.squeeze(sbd.iat[0])
        return tczyx

    def iat_data(self, pi) -> np.ndarray:
        if (data := self._data[pi]) is not None:
            return data
        tczyx = self.tczyx_iat(pi)
        if tczyx.shape[0] > 1:
            raise DataShapeError(f'Cannot return an accessor with multiple time points')
        yxcz = np.moveaxis(
            tczyx[0, :, :, :, :],
            [2, 3, 0, 1],
            [0, 1, 2, 3]
        )
        self._data[pi] = yxcz
        return self.iat_data(pi)

    def iat(self, pi) -> InMemoryDataAccessor:
        return InMemoryDataAccessor(self.iat_data(pi))

    def get_accessors(self):
        return [self.iat(i) for i in range(0, self.count)]

    def pyxcz(self):
        return np.array([self.iat_data(i) for i in range(0, self.count)])

def generate_multiposition_file_accessors(fpath: Path, **kwargs) -> list:
    """
    Given an image file path, return a list of image accessors.
    """
    if str(fpath).upper().endswith('.CZI'):
        return MultiPositionCziFileAccessorSet(fpath, **kwargs)
    else:
        raise FileAccessorError(f'Could not match a multiposition file accessor with {fpath}')

class PatchStack(InMemoryDataAccessor):

    axes = 'PYXCZ'

    def __init__(self, data, force_ydim_longest=False):
        """
        A sequence of n (generally) color 3D images of the same size
        :param data: either a list of np.ndarrays of size YXCZ, or np.ndarray of size PYXCZ
        :param force_ydmin_longest: if creating a PatchStack from a list of different-sized patches, rotate each
            as needed so that height is always greater than or equal to width
        """
        self._slices = []
        if isinstance(data, list):  # list of YXCZ patches
            n = len(data)
            if force_ydim_longest:
                psh = np.array([e.shape[0:2] for e in data]).max(axis=1).max()
                psw = np.array([e.shape[0:2] for e in data]).min(axis=1).max()
                psc, psz = np.array([e.shape[2:] for e in data]).max(axis=0)
                yxcz_shape = np.array([psh, psw, psc, psz])
            else:
                yxcz_shape = np.array([e.shape for e in data]).max(axis=0)
            nda = np.zeros(
                (n, *yxcz_shape), dtype=data[0].dtype
            )
            for i in range(0, len(data)):
                h, w = data[i].shape[0:2]
                if force_ydim_longest and w > h:
                    patch = np.rot90(data[i], axes=(0, 1))
                else:
                    patch = data[i]
                s = tuple([slice(0, c) for c in patch.shape])
                nda[i][s] = patch
                self._slices.append(s)

        elif isinstance(data, np.ndarray) and len(data.shape) == 5:  # interpret as PYXCZ
            nda = data
            for i in range(0, len(data)):
                self._slices.append(tuple([slice(0, c) for c in data[i].shape]))
        else:
            raise InvalidDataForPatchStackError(f'Cannot create accessor from {type(data)}')

        assert nda.ndim == 5
        self.lazy = False
        self._data = nda

    @staticmethod
    def _derived_accessor(data):
        return PatchStack(data)

    def get_slice_at(self, i):
        return self._slices[i]

    def iat(self, i, crop=False):
        if crop:
            return InMemoryDataAccessor(self.data[i, :, :, :, :][self._slices[i]])
        else:
            return InMemoryDataAccessor(self.data[i, :, :, :, :])

    def iat_yxcz(self, i, crop=False):
        return self.iat(i, crop=crop)

    @property
    def count(self):
        return self.shape_dict['P']

    @property
    def data_yx(self) -> np.ndarray:
        if not self.chroma == 1 and self.nz == 1:
            raise InvalidDataShape('Can only return XY array from accessors with a single channel and single z-level')
        else:
            return self.data[:, :, :, 0, 0]

    @property
    def data_yxz(self) -> np.ndarray:
        if not self.chroma == 1:
            raise InvalidDataShape('Can only return XYZ array from accessors with a single channel')
        else:
            return self.data[:, :, :, 0, :]

    @property
    def smallest(self):
        return np.array([p.shape for p in self.get_list()]).min(axis=0),

    @property
    def largest(self):
        return np.array([p.shape for p in self.get_list()]).max(axis=0)

    def export_pyxcz(self, fpath: Path):
        tzcyx = np.moveaxis(
            self.pyxcz,  # yxcz
            [0, 4, 3, 1, 2],
            [0, 1, 2, 3, 4]
        )

        if self.is_mask():
            if self.dtype == 'bool':
                data = (tzcyx * 255).astype('uint8')
            else:
                data = tzcyx.astype('uint8')
            tifffile.imwrite(fpath, data, imagej=True)
        else:
            tifffile.imwrite(fpath, tzcyx, imagej=True)

    @classmethod
    def read(cls, fp: Path):
        try:
            tf = tifffile.TiffFile(fp)
        except Exception:
            raise FileAccessorError(f'Unable to access data in {fp}')

        if len(tf.series) != 1:
            raise DataShapeError(f'Expect only one series in {fp}')

        se = tf.series[0]

        order = ['T', 'Y', 'X', 'C', 'Z']
        axs = [a for a in se.axes if a in order]

        pyxcz = np.moveaxis(
            se.asarray(),
            [axs.index(k) for k in order],
            [0, 1, 2, 3, 4]
        )

        return cls(pyxcz)

    def write(self, fp: Path, mkdir=True, single_file=True) -> Path:
        if mkdir:
            fp.parent.mkdir(parents=True, exist_ok=True)
        if single_file:
            self.export_pyxcz(fp)
            return [fp]
        else:
            paths = []
            for i, patch_data in enumerate(self.get_list(crop=True)):
                new_path = fp.parent / f'{fp.stem}-P{i:04d}{fp.suffix}'
                InMemoryDataAccessor(patch_data).write(new_path)
                paths.append(new_path)
        return paths

    @property
    def shape_dict(self):
        return dict(zip(('P', 'Y', 'X', 'C', 'Z'), self.data.shape))

    def get_channels(self, channels: list, mip: bool = False):
        nda = super().get_channels(channels, mip).data
        return self._derived_accessor([nda[i][self._slices[i]] for i in range(0, self.count)])

    def get_list(self, crop=True):
        if crop:
            return [self.data[i][self._slices[i]] for i in range(0, self.count)]
        else:
            return [self.data[i] for i in range(0, self.count)]

    @property
    def pyxcz(self):
        return self.data

    @property
    def pczyx(self):
        return np.moveaxis(
            self.data,
            [0, 3, 4, 1, 2],
            [0, 1, 2, 3, 4]
        )

    @property
    def shape(self):
        return self.data.shape

    @property
    def shape_dict(self):
        return dict(zip(('P', 'Y', 'X', 'C', 'Z'), self.data.shape))

    def get_object_df(self, mask) -> pd.DataFrame:
        """
        Given a mask patch stack of the same size, return a DataFrame summarizing the area and intensity of objects,
        assuming the each patch in the patch stack represents a single object.
        :param mask of the same dimensions
        """
        if not mask.can_mask(self):
            raise DataShapeError(f'Patch stack object dataframe expects a mask of the same dimensions')
        df = pd.DataFrame([
            {
                'label': i,
                'area': (mask.iat(i).data > 0).sum(),
                'intensity_sum': (self.iat(i).data * (mask.iat(i).data > 0)).sum()
            } for i in range(0, self.count)
        ])
        df['intensity_mean'] = df['intensity_sum'] / df['area']
        return df


def make_patch_stack_from_file(fpath):  # interpret t-dimension as patch position
    if not Path(fpath).exists():
        raise FileNotFoundError(f'Could not find {fpath}')

    try:
        tf = tifffile.TiffFile(fpath)
    except Exception:
        raise FileAccessorError(f'Unable to access data in {fpath}')

    if len(tf.series) != 1:
        raise DataShapeError(f'Expect only one series in {fpath}')

    se = tf.series[0]

    axs = [a for a in se.axes if a in [*'TZCYX']]
    sd = dict(zip(axs, se.shape))
    for a in [*'TZC']:
        if a not in axs:
            sd[a] = 1
    tzcyx = se.asarray().reshape([sd[k] for k in [*'TZCYX']])

    pyxcz = np.moveaxis(
        tzcyx,
        [0, 3, 4, 2, 1],
        [0, 1, 2, 3, 4],
    )
    return PatchStack(pyxcz)


class Error(Exception):
    pass

class FileAccessorError(Error):
    pass

class FileNotFoundError(Error):
    pass

class DataShapeError(Error):
    pass

class DataTypeError(Error):
    pass

class FileWriteError(Error):
    pass

class InvalidAxisKey(Error):
    pass

class InvalidCziCompression(Error):
    pass

class InvalidDataShape(Error):
    pass

class InvalidDataForPatchStackError(Error):
    pass



