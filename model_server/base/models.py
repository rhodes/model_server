from abc import ABC, abstractmethod

import numpy as np

from .accessors import GenericImageDataAccessor, PatchStack

class Model(ABC):

    def __init__(self, autoload: bool = True, info: dict = None):
        """
        Abstract base class for an inference model that uses image data as an input.
        :param autoload: automatically load model and dependencies into memory if True
        :param info: optional dictionary of JSON-serializable information to report to API
        """
        self.autoload = autoload
        self.loaded = False
        self._info = info
        if not autoload:
            return None
        if self.load():
            self.loaded = True
        else:
            raise CouldNotLoadModelError()
        return None

    @property
    def info(self):
        return self._info

    @abstractmethod
    def load(self):
        """
        Abstract method that carries out the expectedly time-consuming step of loading a model into memory
        :return: True if successful, else False
        """
        pass

    @abstractmethod
    def infer(self, *args) -> object:
        """
        Abstract method that carries out the computationally intensive step of running data through a model
        :param args:
        :return:
        """
        pass

    def reload(self):
        self.load()

    @property
    def name(self):
        return f'{self.__class__.__name__}'



class ImageToImageModel(Model):
    """
    Abstract class for models that receive an image and return an image of the same size
    """

    @abstractmethod
    def infer(self, img: GenericImageDataAccessor, *args, **kwargs) -> GenericImageDataAccessor:
        pass


class SemanticSegmentationModel(ImageToImageModel):
    """
    Base model that exposes a method that returns a binary mask for a given input image and pixel class
    """

    @abstractmethod
    def label_pixel_class(
            self, img: GenericImageDataAccessor, **kwargs) -> GenericImageDataAccessor:
        """
        Given an image, return an image of the same shape where each pixel is assigned to one or more integer classes
        """
        pass

    def label_patch_stack(self, img: PatchStack, **kwargs) -> PatchStack:
        """
        Iterative over a patch stack, call pixel labeling (to boolean array) separately on each cropped patch
        """
        data = np.zeros((img.count, *img.hw, 1, img.nz), dtype=bool)
        for i in range(0, img.count):
            sl = img.get_slice_at(i)
            data[i][sl] = self.label_pixel_class(img.iat(i, crop=True), **kwargs)
        return PatchStack(data)


class BinaryThresholdSegmentationModel(SemanticSegmentationModel):
    def __init__(self, tr: float = 0.5, channel: int = 0):
        """
        Model that labels all pixels as class 1 if the intensity in specified channel exceeds a threshold.
        :param tr: threshold in range of 0.0 to 1.0; model handles normalization to full pixel intensity range
        :param channel: channel to use for thresholding
        """
        self.tr = tr
        self.channel = channel
        self.loaded = self.load()
        super().__init__(info={'tr': tr, 'channel': channel})

    def infer(self, acc: GenericImageDataAccessor) -> GenericImageDataAccessor:
        norm_tr = self.tr * acc.dtype_max
        return acc.apply(lambda x: x > norm_tr)

    def label_pixel_class(self, acc: GenericImageDataAccessor, **kwargs) -> GenericImageDataAccessor:
        return self.infer(acc, **kwargs)

    def load(self):
        return True


class InstanceMaskSegmentationModel(ImageToImageModel):
    """
    Base model that exposes a method that returns an instance classification map for a given input image and mask
    """

    @abstractmethod
    def infer(self, img: GenericImageDataAccessor, mask: GenericImageDataAccessor, **kwargs) -> GenericImageDataAccessor:
        pass

    @abstractmethod
    def label_instance_class(
            self, img: GenericImageDataAccessor, mask: GenericImageDataAccessor, **kwargs
    ) -> GenericImageDataAccessor:
        """
        Given an image and a mask of the same size, return a map where each connected object is assigned a class
        """
        if not mask.is_mask():
            raise InvalidInputImageError('Expecting a binary mask')
        if img.hw != mask.hw or img.nz != mask.nz:
            raise InvalidInputImageError('Expect input image and mask to be the same shape')

        return self.infer(img, mask)

    def label_patch_stack(self, img: PatchStack, mask: PatchStack, allow_multiple=True, force_single=False, **kwargs):
        """
        Call inference on all patches in a PatchStack at once
        :param img: raw image data
        :param mask: binary masks of same shape as img
        :param allow_multiple: allow multiple nonzero pixel values in inferred patches if True
        :param force_single: if True, and if allow_multiple is False, convert all nonzero pixels in a patch to the most
            used label; otherwise raise an exception
        :return: PatchStack of labeled objects
        """
        res = self.label_instance_class(img, mask, **kwargs)
        data = res.data
        for i in range(0, res.count): # interpret as PYXCZ
            la_patch = data[i, :, :, :, :]
            la, ct = np.unique(la_patch, return_counts=True)
            if len(la[la > 0]) > 1 and not allow_multiple:
                if force_single:
                     la_patch[la_patch > 0] = la[1:][ct[1:].argsort()[-1]]  # most common nonzero value
                else:
                    raise InvalidObjectLabelsError(f'Found more than one nonzero label: {la}, counts: {ct}')
            data[i, :, :, :, :] = la_patch
        return PatchStack(data)


class Error(Exception):
    pass

class CouldNotLoadModelError(Error):
    pass

class ParameterExpectedError(Error):
    pass

class InvalidInputImageError(Error):
    pass

class InvalidObjectLabelsError(Error):
    pass