"""
Image processing utility functions
"""
from math import ceil, floor

import numpy as np
import skimage
from skimage.exposure import rescale_intensity
from skimage.filters import gaussian
from skimage.measure import find_contours

def is_mask(img):
    """
    Return True if an image represents a binary mask
    :param img: np.ndarray
    """
    if img.dtype == 'bool':
        return True
    elif img.dtype in ['uint8', 'uint16']:
        unique = np.unique(img)
        if unique.shape[0] <= 2 and all([u in [0, 255] for u in unique]):
            return True
        elif unique.shape[0] <= 2 and all([u in [0, 1] for u in unique]):
            return True
    return False

def pad(yxcz, mpx: int):
    """
    Pad and crop image data in Y and X axes to meet specific dimension
    :param yxcz: np.ndarray
    :param mpx: int pixel size of resulting square
    :return: np.ndarray array of size (mpx, mpx, nc, nz)
    """
    assert len(yxcz.shape) == 4
    dh = 0.5 * (mpx - yxcz.shape[0])
    dw = 0.5 * (mpx - yxcz.shape[1])

    if dw < 0:
        x0 = floor(-dw)
        x1 = x0 + mpx
        yxcz = yxcz[:, x0:x1, :, :]
        dw = 0
    if dh < 0:
        y0 = floor(-dh)
        y1 = y0 + mpx
        yxcz = yxcz[y0:y1, :, :, :]
        dh = 0

    border = ((floor(dh), ceil(dh)), (floor(dw), ceil(dw)), (0, 0), (0, 0))
    padded = np.pad(yxcz, border, mode='constant')
    return padded

def resample_to_8bit(nda, cmin=0, cmax=2**16):
    """
    Resample a 16 bit image to 8 bit, optionally bracketing a given intensity range
    :param nda: np.ndarray input data of arbitrary dimension
    :param cmin: intensity level on 16-bit scale that become zero in 8-bit scale
    :param cmax: intensity level on 16-bit scale that become maximum (255) in 8-bit scale
    :return: rescaled data of same dimension as input
    """
    return rescale_intensity(
        np.clip(nda, cmin, cmax),
        in_range=(cmin, cmax + 1),
        out_range=(0, 2**8)
    ).astype('uint8')


def rescale(nda, clip=0.0):
    """
    Rescale an image for a given clipping ratio
    :param nda: input data of arbitrary dimension and scale
    :param clip: Ratio of clipping in the resulting image
    :return: rescaled image of same dimension as input
    """
    clip_pct = (100.0 * clip, 100.0 * (1.0 - clip))
    cmin, cmax = np.percentile(nda, clip_pct)
    rescaled = rescale_intensity(nda, in_range=(cmin, cmax))
    return rescaled


def make_rgb(nda):
    """
    Convert a YXCZ stack array to RGB, and error if more than three channels
    :param nda: np.ndarray (YXCZ dimensions)
    :return: np.ndarray of 3-channel stack
    """
    h, w, c, nz = nda.shape
    assert c <= 3
    outdata = np.zeros((h, w, 3, nz), dtype=nda.dtype)
    outdata[:, :, 0:c, :] = nda[:, :, :, :]
    return outdata


def mask_largest_object(
        img: np.ndarray,
        max_allowed: int = 10,
        verbose: bool = True
) -> np.ndarray:
    """
    Where more than one connected component is found in an image, return the largest object by area
    :param img: (np.ndarray) containing object labels or binary mask
    :param max_allowed: raise an error if more than this number of objects is found
    :param verbose: print a message each time more than one object is found
    :return: np.ndarray of same size as img
    """
    if is_mask(img): # assign object labels
        ob_id = skimage.measure.label(img)
    else:  # assume img is contains object labels
        ob_id = img

    num_obj = len(np.unique(ob_id)) - 1
    if num_obj > max_allowed:
        raise TooManyObjectError(f'Found {num_obj} objects in frame')
    if num_obj > 1:
        if verbose:
            print(f'Found {num_obj} nonzero unique values in object map; keeping the one with the largest area')
        val, cts = np.unique(ob_id, return_counts=True)
        mask = ob_id == val[1 + cts[1:].argmax()]
        return mask * img
    else:
        return img

def get_safe_contours(mask):
    """
    Return a list of contour coordinates even if a mask is only one pixel across
    """
    if mask.shape[0] == 1 or mask.shape[1] == 1:
        c0 = mask.shape[0] - 1
        c1 = mask.shape[1] - 1
        return [np.array([(0, 0), (c0, c1)])]
    else:
        return find_contours(mask)

def smooth(img: np.ndarray, sig: float, tr: float = 0.5) -> np.ndarray:
    """
    Perform Gaussian smoothing on an image
    :param img: image data
    :param sig: threshold parameter
    :param tr: if img is a binary mask, re-binarize the floating-point gaussian result above this threshold
    :return: smoothed image
    """
    ga = gaussian(img, sig, preserve_range=True)
    if is_mask(img):
        if img.dtype == 'bool':
            return ga > tr
        elif img.dtype == 'uint8':
            return (255 * (ga > tr)).astype('uint8')
    else:
        return ga.astype(img.dtype)

class Error(Exception):
    pass


class TooManyObjectError(Exception):
    pass


def safe_add(a, g, b):
    assert a.dtype == b.dtype
    assert a.shape == b.shape
    assert g >= 0.0

    return np.clip(
        a.astype('uint32') + g * b.astype('uint32'),
        0,
        np.iinfo(a.dtype).max
    ).astype(a.dtype)
