"""
Classes that manage data sharing between server and outside processes.  Currently just a directory on a shared
filesystem; but may expand to include data transfer over API, shared memory objects, etc.
"""

import os
import pathlib

def validate_directory_exists(path):
    return os.path.exists(path)

class SharedImageDirectory(object):

    def __init__(self, path: pathlib.Path):
        self.path = path
        self.is_memory_mapped = False

        if not validate_directory_exists(path):
            raise InvalidDirectoryError(f'Invalid directory:\n{path}')

class InvalidDirectoryError(Exception):
    pass

