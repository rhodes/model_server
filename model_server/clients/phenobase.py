from pathlib import Path

import pandas as pd

from model_server.rois.phenobase import PhenoBase

from model_server.clients.batch_runner import FileBatchRunnerClient

class MakePhenoBaseClient(FileBatchRunnerClient):

    def __init__(self, *args, labels_csv: Path = None, **kwargs):
        """
        Create a batch runner client that outputs a PhenoBase in its output directory
        :param args:
        :param labels_csv:
            optional path to a CSV file that relates object labels to substrings matched on each input stack
        :param kwargs:
        """
        self.labels_csv_path = labels_csv
        return super().__init__(*args, **kwargs)

    def get_stacks(self, *args, **kwargs):
        df_stacks = super().get_stacks(*args, **kwargs)
        if self.labels_csv_path is not None:
            labels = pd.read_csv(self.labels_csv_path)
            def get_label(pa):
                for row in labels.itertuples():
                    if row.pattern in str(pa):
                        return row.label
                return None
            df_stacks['category_label'] = df_stacks.local_path.apply(get_label)
        return df_stacks