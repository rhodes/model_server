from pathlib import Path

root = Path.home() / 'svlt' / 'sessions'

subdirectories = {
    'conf': 'conf',
    'logs': 'logs',
    'inbound_images': 'images/inbound',
    'outbound_images': 'images/outbound',
    'rois': 'rois',
    'tables': 'tables',
}
server_conf = {
    'host': '127.0.0.1',
    'port': 8000,
}
