import importlib

from ..base.api import app

for ex in ['ilastik', 'rois']:
    m = importlib.import_module(f'..{ex}.router', package=__package__)
    app.include_router(m.router)
