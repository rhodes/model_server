from multiprocessing import Process
import os
import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry
import uvicorn
import webbrowser


def main(host, port, confpath, reload, debug, root) -> None:

    if root:
        os.environ['SVLT_SESSION_ROOT'] = root

    server_process = Process(
        target=uvicorn.run,
        args=(f'{confpath}:app',),
        kwargs={
            'app_dir': '..',
            'host': host,
            'port': int(port),
            'log_level': 'debug',
            'reload': reload,
        },
        daemon=(reload is False),
    )
    url = f'http://{host}:{int(port):04d}/'
    print(url)
    server_process.start()

    try:
        sesh = requests.Session()
        retries = Retry(
            total=10,
            backoff_factor=0.5,
        )
        sesh.mount('http://', HTTPAdapter(max_retries=retries))
        resp = sesh.get(url + 'status')
        assert resp.status_code == 200
    except Exception:
        print('Error starting server')
        server_process.terminate()
        exit()

    webbrowser.open(url + 'status', new=1, autoraise=True)

    if debug:
        print('Running in debug mode')
    print('Type "STOP" to stop server')
    input_str = ''
    while input_str.upper() != 'STOP':
        input_str = input()
    session_path = requests.get(url + 'paths/session').text

    print(f'Terminating server.\nSession data are located in {session_path}')

    server_process.terminate()
    return session_path
