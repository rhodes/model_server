import json
import os
import unittest
from math import floor
from multiprocessing import Process
from pathlib import Path
from shutil import copyfile

from fastapi import APIRouter
import numpy as np
from pydantic import BaseModel

from .fastapi import app
from ..base.accessors import GenericImageDataAccessor, InMemoryDataAccessor
from ..base.models import SemanticSegmentationModel, InstanceMaskSegmentationModel
from ..base.pipelines.shared import call_pipeline, PipelineParams, PipelineQueueRecord, PipelineTrace
from ..base.session import session
from ..clients.py3 import HttpClient

from ..base.accessors import generate_file_accessor

"""
Configure additional endpoints for testing
"""
test_router = APIRouter(prefix='/testing', tags=['testing'])

class BounceBackParams(BaseModel):
    par1: str
    par2: list

@test_router.put('/bounce_back')
def list_bounce_back(params: BounceBackParams):
    return {'success': True, 'params': {'par1': params.par1, 'par2': params.par2}}

@test_router.put('/accessors/dummy_accessor/load')
def load_dummy_accessor() -> str:
    acc = InMemoryDataAccessor(
        np.random.randint(
            0,
            2 ** 8,
            size=(512, 256, 3, 7),
            dtype='uint8'
        )
    )
    return session.add_accessor(acc)

@test_router.put('/models/dummy_semantic/load/')
def load_dummy_semantic_model() -> dict:
    mid = session.load_model(DummySemanticSegmentationModel)
    session.log_info(f'Loaded model {mid}')
    return {'model_id': mid}

@test_router.put('/models/dummy_instance/load/')
def load_dummy_instance_model() -> dict:
    mid = session.load_model(DummyInstanceMaskSegmentationModel)
    session.log_info(f'Loaded model {mid}')
    return {'model_id': mid}


class DummyTaskParams(PipelineParams):
    accessor_id: str
    break_me: bool = False

@test_router.put('/tasks/create_dummy_task')
def create_dummy_task(params: DummyTaskParams) -> PipelineQueueRecord:
    def _dummy_pipeline(accessors, models,  **k):
        d = PipelineTrace(accessors.get(''))
        if k.get('break_me'):
            raise Exception('I broke')
        model = models.get('')
        d['plus_one'] = d.last.apply(lambda x: x + 1)
        d['res'] = d.last.apply(lambda x: 2 * x)
        return d

    task_id = session.tasks.add_task(
        lambda x: call_pipeline(_dummy_pipeline, x),
        params
    )
    return PipelineQueueRecord(task_id=task_id)

app.include_router(test_router)


class TestServerBaseClass(unittest.TestCase):
    """
    Base class for unittests of API functionality.  Implements both server and clients for testing.
    """

    app_name = 'model_server.conf.testing:app'

    def setUp(self) -> None:
        import uvicorn
        host = '127.0.0.1'
        port = 5001

        self.server_process = Process(
            target=uvicorn.run,
            args=(self.app_name, ),
            kwargs={'host': host, 'port': port, 'log_level': 'critical'},
            daemon=True
        )
        self.server_process.start()
        self.client = HttpClient(host=host, port=port)

    def assertGetSuccess(self, endpoint):
        resp = self.client.get(endpoint)
        self.assertEqual(resp.status_code, 200, resp.text)
        return resp.json()

    def assertGetFailure(self, endpoint, code):
        resp = self.client.get(endpoint)
        self.assertEqual(resp.status_code, code)
        return resp

    def assertPutSuccess(self, endpoint, query={}, body={}):
        resp = self.client.put(
            endpoint,
            query=query,
            body=body
        )
        self.assertEqual(resp.status_code, 200, resp.text)
        return resp.json()

    def assertPutFailure(self, endpoint, code, query=None, body=None):
        resp = self.client.put(
            endpoint,
            query=query,
            body=body
        )
        self.assertEqual(resp.status_code, code)
        return resp

    def tearDown(self) -> None:
        self.server_process.terminate()
        self.server_process.join()

    def copy_input_file_to_server(self):
        r = self.assertGetSuccess('paths')
        pa = r['inbound_images']
        copyfile(
            self.input_data['path'],
            Path(pa) / self.input_data['name']
        )
        return self.input_data['name']

    def get_accessor(self, accessor_id, filename=None, copy_to=None):
        r = self.assertPutSuccess(
            f'/accessors/write_to_file/{accessor_id}',
            query={
                'filename': filename,
                'pop': False,
            },
        )
        fp_out = Path(self.assertGetSuccess('paths')['outbound_images']) / r
        self.assertTrue(fp_out.exists())
        if copy_to:
            copyfile(fp_out, Path(copy_to) / f'normal_{fp_out.name}')
        return generate_file_accessor(fp_out)


def setup_test_data():
    """
    Look for test data, create test output directory, parse and return meta information
    :return:
        meta (dict) of test data and paths
    """

    def _winpath(f):
        if not isinstance(f, str):
            return f
        p = f.split('/')
        if len(p) > 1:
            p[1] = p[1] + ':'
            return '\\'.join(p[1:])
        else:
            return f

    # places to look for test data
    data_paths = [
        os.environ.get('UNITTEST_DATA_ROOT'),
        _winpath(os.environ.get('UNITTEST_DATA_ROOT')),
        Path.home() / 'model_server' / 'testing',
        os.getcwd(),
    ]
    root = None

    # look for first instance of summary.json
    for dp in data_paths:
        if dp is None:
            continue
        sf = (Path(dp) / 'summary.json')
        if sf.exists():
            with open(sf, 'r') as fh:
                meta = json.load(fh)
                root = dp
                break

    if root is None:
        raise Exception('Could not find test data, try setting environmental variable UNITTEST_DATA_ROOT.')

    meta['root'] = Path(root)
    meta['output_path'] = meta['root'] / 'test_output'
    meta['output_path'].mkdir(parents=True, exist_ok=True)

    # resolve relative paths
    def _resolve_paths(d):
        keys = list(d.keys())
        for k in keys:
            if k == 'name':
                d['path'] = meta['root'] / d['name']
            elif isinstance(d[k], dict):
                _resolve_paths(d[k])
    _resolve_paths(meta)

    return meta

# object containing test data paths and metadata, for import into unittest modules
meta = setup_test_data()


class DummySemanticSegmentationModel(SemanticSegmentationModel):

    model_id = 'dummy_make_white_square'

    def load(self):
        return True

    def infer(self, img: GenericImageDataAccessor) -> GenericImageDataAccessor:
        super().infer(img)
        w = img.shape_dict['X']
        h = img.shape_dict['Y']
        result = np.zeros([h, w], dtype='uint8')
        result[floor(0.25 * h) : floor(0.75 * h), floor(0.25 * w) : floor(0.75 * w)] = 255
        return InMemoryDataAccessor(data=result), {'success': True}

    def label_pixel_class(
            self, img: GenericImageDataAccessor, **kwargs) -> GenericImageDataAccessor:
        mask, _ = self.infer(img)
        return mask


class DummyInstanceMaskSegmentationModel(InstanceMaskSegmentationModel):

    model_id = 'dummy_pass_input_mask'

    def load(self):
        return True

    def infer(
            self, img: GenericImageDataAccessor, mask: GenericImageDataAccessor
    ) -> (GenericImageDataAccessor, dict):
        return img.__class__(
            (mask.data / mask.data.max()).astype('uint16')
        )

    def label_instance_class(
            self, img: GenericImageDataAccessor, mask: GenericImageDataAccessor, **kwargs
    ) -> GenericImageDataAccessor:
        """
        Returns a trivial segmentation, i.e. the input mask with value 1
        """
        super(DummyInstanceMaskSegmentationModel, self).label_instance_class(img, mask, **kwargs)
        return self.infer(img, mask)
