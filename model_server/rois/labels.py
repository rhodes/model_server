from math import sqrt

import numpy as np
import pandas as pd
from scipy.stats import moment
from skimage.filters import sobel
from skimage.measure import label, shannon_entropy, regionprops_table

from model_server.base.accessors import GenericImageDataAccessor, InMemoryDataAccessor
from model_server.rois.df import filter_df, insert_level, is_df_3d, df_insert_slices


def get_label_ids(acc_seg_mask: GenericImageDataAccessor, allow_3d=False, connect_3d=True) -> InMemoryDataAccessor:
    """
    Convert binary segmentation mask into either a 2D or 3D object identities map
    :param acc_seg_mask: binary segmentation mask (mono) of either two or three dimensions
    :param allow_3d: return a 3D map if True; return a 2D map of the mask's maximum intensity project if False
    :param connect_3d: objects can span multiple z-positions if True; objects are unique to a single z if False
    :return: object identities map
    """
    if allow_3d and connect_3d:
        nda_la = label(
            acc_seg_mask.data_yxz,
            connectivity=3,
        ).astype('uint16')
        return InMemoryDataAccessor(np.expand_dims(nda_la, 2))
    elif allow_3d and not connect_3d:
        nla = 0
        la_3d = np.zeros((*acc_seg_mask.hw, 1, acc_seg_mask.nz), dtype='uint16')
        for zi in range(0, acc_seg_mask.nz):
            la_2d = label(
                acc_seg_mask.data_yxz[:, :, zi],
                connectivity=2,
            ).astype('uint16')
            la_2d[la_2d > 0] = la_2d[la_2d > 0] + nla
            nla = la_2d.max()
            la_3d[:, :, 0, zi] = la_2d
        return InMemoryDataAccessor(la_3d)
    else:
        return InMemoryDataAccessor(
            label(
                acc_seg_mask.get_mip().data_yx,
                connectivity=1,
            ).astype('uint16')
        )


def focus_metrics():
    return {
        'max_intensity': lambda x: np.max(x),
        'stdev': lambda x: np.std(x),
        'max_sobel': lambda x: np.max(sobel(x)),
        'rms_sobel': lambda x: sqrt(np.mean(sobel(x) ** 2)),
        'entropy': lambda x: shannon_entropy(x),
        'moment': lambda x: moment(x.flatten(), moment=2),
    }


def make_df_from_object_ids(
        acc_raw,
        acc_obj_ids,
        expand_box_by,
        deproject_channel=None,
        filters=None,
        deproject_intensity_threshold=0.0
) -> pd.DataFrame:
    """
    Build dataframe that associate object IDs with summary stats;
    :param acc_raw: accessor to raw image data
    :param acc_obj_ids: accessor to map of object IDs
    :param expand_box_by: number of pixels to expand bounding box in all directions (without exceeding image boundary)
    :param deproject_channel: if objects' z-coordinates are not specified, compute them based on argmax of this channel
    :param deproject_intensity_threshold: when deprojecting, round MIP deprojection_channel to zero if below this
        threshold (as fraction of full range, 0.0 to 1.0)
    :return: pd.DataFrame
    """
    # build dataframe of objects, assign z index to each object

    if acc_obj_ids.nz == 1 and acc_raw.nz > 1:  # apply deprojection

        if deproject_channel is None or deproject_channel >= acc_raw.chroma or deproject_channel < 0:
            if acc_raw.chroma == 1:
                deproject_channel = 0
            else:
                raise NoDeprojectChannelSpecifiedError(
                    f'When labeling objects, either their z-coordinates or a valid deprojection channel are required.'
                )

        mono = acc_raw.get_mono(deproject_channel)
        intensity_weight = mono.get_mip().data_yx.astype('uint16')
        intensity_weight[intensity_weight < (deproject_intensity_threshold * mono.dtype_max)] = 0
        argmax = mono.get_z_argmax().data_yx.astype('uint16')
        zi_map = np.stack([
            intensity_weight,
            argmax * intensity_weight,
        ], axis=-1)

        assert len(zi_map.shape) == 3
        df = pd.DataFrame(regionprops_table(
            acc_obj_ids.data_yx,
            intensity_image=zi_map,
            properties=('label', 'area', 'intensity_mean', 'bbox')
        )).rename(columns={'bbox-0': 'y0', 'bbox-1': 'x0', 'bbox-2': 'y1', 'bbox-3': 'x1'})

        df['zi'] = (df['intensity_mean-1'] / df['intensity_mean-0']).fillna(0).round().astype('int16')
        df = df.drop(['intensity_mean-0', 'intensity_mean-1'], axis=1)

        def _make_binary_mask(r):
            acc = InMemoryDataAccessor(acc_obj_ids.data == r.name)
            cropped = acc.get_mono(0, mip=True).crop_hw(
                (int(r.y0), int(r.x0), int(r.y1 - r.y0), int(r.x1 - r.x0))
            ).data_yx
            return cropped

    elif acc_obj_ids.nz == 1 and acc_raw.nz == 1:  # purely 2d, no z information in dataframe
        df = pd.DataFrame(regionprops_table(
            acc_obj_ids.data_yx,
            properties=('label', 'area', 'bbox')
        )).rename(columns={
            'bbox-0': 'y0', 'bbox-1': 'x0', 'bbox-2': 'y1', 'bbox-3': 'x1'
        })

        def _make_binary_mask(r):
            acc = InMemoryDataAccessor(acc_obj_ids.data == r.name)
            cropped = acc.get_mono(0).crop_hw(
                (int(r.y0), int(r.x0), int(r.y1 - r.y0), int(r.x1 - r.x0))
            ).data_yx
            return cropped

    else:  # purely 3d: objects' median z-coordinates come from arg of max count in object identities map
        df = pd.DataFrame(regionprops_table(
            acc_obj_ids.data_yxz,
            properties=('label', 'area', 'bbox')
        )).rename(columns={
            'bbox-0': 'y0', 'bbox-1': 'x0', 'bbox-2': 'z0', 'bbox-3': 'y1', 'bbox-4': 'x1', 'bbox-5': 'z1'
        })

        def _get_zi_from_label(r):
            r = r.convert_dtypes()
            la = r.name
            crop = acc_obj_ids.crop_hwd((r.y0, r.x0, r.z0, r.y1 - r.y0, r.x1 - r.x0, r.z1 - r.z0))
            rel_argzmax = crop.apply(lambda x: x == la).get_focus_vector().argmax()
            return rel_argzmax + r.z0

        df['zi'] = df.apply(_get_zi_from_label, axis=1, result_type='reduce')
        df['nz'] = df['z1'] - df['z0']

        def _make_binary_mask(r):
            r = r.convert_dtypes()
            la = r.name
            crop = acc_obj_ids.crop_hwd(
                (int(r.y0), int(r.x0), int(r.z0), int(r.y1 - r.y0), int(r.x1 - r.x0), int(r.z1 - r.z0))
            )
            return crop.apply(lambda x: x == la).data_yxz
    df = df.set_index('label')
    insert_level(df, 'bounding_box')
    df = df_insert_slices(df, acc_raw.shape_dict, expand_box_by)
    filters_dict = {} if filters is None else filters.dict(exclude_unset=True)
    df_fil = filter_df(df, filters_dict)
    df_fil['masks', 'binary_mask'] = df_fil.bounding_box.apply(
        _make_binary_mask,
        axis=1,
        result_type='reduce',
    )
    return df_fil


def make_object_ids_from_df(df: pd.DataFrame, sd: dict) -> InMemoryDataAccessor:
    id_mask = np.zeros((sd['Y'], sd['X'], 1, sd['Z']), dtype='uint16')

    if 'binary_mask' not in df.masks.columns:
        raise MissingSegmentationError('RoiSet dataframe does not contain segmentation')

    if is_df_3d(df):  # use 3d coordinates
        def _label_obj(r):
            bb = r.bounding_box
            sl = np.s_[bb.y0:bb.y1, bb.x0:bb.x1, :, bb.z0:bb.z1]
            mask = np.expand_dims(r.masks.binary_mask, 2)
            id_mask[sl] = id_mask[sl] + r.name * mask
    elif 'zi' in df.bounding_box.columns:
        def _label_obj(r):
            bb = r.bounding_box
            sl = np.s_[bb.y0:bb.y1, bb.x0:bb.x1, :, bb.zi: (bb.zi + 1)]
            mask = np.expand_dims(r.masks.binary_mask, (2, 3))
            id_mask[sl] = id_mask[sl] + r.name * mask
    else:
        def _label_obj(r):
            bb = r.bounding_box
            sl = np.s_[bb.y0:bb.y1, bb.x0:bb.x1, :]
            mask = np.expand_dims(r.masks.binary_mask, (2, 3))
            id_mask[sl] = id_mask[sl] + r.name * mask

    df.apply(_label_obj, axis=1)
    return InMemoryDataAccessor(id_mask)


class Error(Exception):
    pass


class NoDeprojectChannelSpecifiedError(Error):
    pass


class MissingSegmentationError(Error):
    pass
