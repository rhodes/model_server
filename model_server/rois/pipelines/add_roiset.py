from typing import Dict, Union

from fastapi import APIRouter
from pydantic import Field

from model_server.base.accessors import GenericImageDataAccessor
from model_server.base.models import Model
from model_server.base.pipelines.shared import call_roiset_pipeline, RoiSetPipelineParams, PipelineQueueRecord, PipelineRecord, PipelineTrace
from model_server.rois.roiset import RoiSet, RoiSetMetaParams


class AddRoiSetParams(RoiSetPipelineParams):

    accessor_id: str = Field(
        description='ID of raw data to use in RoiSet'
    )
    labels_accessor_id: str = Field(
        description='ID of label mask to use in RoiSet'
    )
    roi_params: RoiSetMetaParams = RoiSetMetaParams(**{
        'mask_type': 'boxes',
        'filters': {
            'area': {'min': 1e3, 'max': 1e8}
        },
        'expand_box_by': [0, 0],
        'deproject_channel': None,
    })

class SegToRoiSetRecord(PipelineRecord):
    pass


router = APIRouter()


@router.put('/add_roiset')
def seg_to_roiset(p: AddRoiSetParams) -> Union[SegToRoiSetRecord, PipelineQueueRecord]:
    """
    Compute a RoiSet from 2d segmentation, apply to z-stack, and optionally apply object classification.
    """
    return call_roiset_pipeline(add_roiset_pipeline, p)


def add_roiset_pipeline(
        accessors: Dict[str, GenericImageDataAccessor],
        models: Dict[str, Model],
        **k
) -> PipelineTrace:
    d = PipelineTrace(accessors[''])
    d['labels'] = accessors['labels_']
    rois = RoiSet.from_object_ids(
        d['input'],
        d['labels'],
        RoiSetMetaParams(**k['roi_params'])
    )

    for patch_series, patch_params in k['patches'].items():
        d[patch_series] = rois.get_patches_acc(**patch_params)
    d['patch_masks'] = rois.get_patch_masks_acc()


    return d, rois