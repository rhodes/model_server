import importlib

from fastapi import APIRouter

router = APIRouter(
    prefix='/rois/pipelines',
    tags=['pipelines'],
)

for m in ['roiset_obmap', 'add_roiset']:
    router.include_router(
        importlib.import_module(
            f'{__package__}.pipelines.{m}'
        ).router
    )