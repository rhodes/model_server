import argparse

from model_server.conf.defaults import root, server_conf
from model_server.conf.startup import main

def parse_args():
    parser = argparse.ArgumentParser(
        description='Start model server with optional arguments',
    )
    parser.add_argument(
        '--confpath',
        default='model_server.conf.fastapi',
        help='path to server startup configuration',
    )
    parser.add_argument(
        '--host',
        default=server_conf['host'],
        help='bind socket to this host'
    )
    parser.add_argument(
        '--port',
        default=str(server_conf['port']),
        help='bind socket to this port',
    )
    parser.add_argument(
        '--root',
        default=root.__str__(),
        help='root directory of session data'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='display extra information that is helpful for debugging'
    )
    parser.add_argument(
        '--reload',
        action='store_true',
        help='automatically restart server when changes are noticed, for development purposes'
    )

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    print('CLI args:\n' + str(args))
    main(**args.__dict__)
    print('Finished')

